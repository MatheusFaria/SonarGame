﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SonarBeep : MonoBehaviour {

    public float speed = 0.3f;

    private float maxScale = 1.2f;

	void Start ()
    {
	}
	
	void Update ()
    {
        if (transform.localScale.x >= maxScale)
        {
            gameObject.SetActive(false);
            Destroy(gameObject);
        }
        else
            transform.localScale += new Vector3(speed * Time.deltaTime, speed * Time.deltaTime, 0);
	}

    void OnTriggerEnter2D(Collider2D collider)
    {
        SonarDiscoverFade obj = collider.gameObject.GetComponent<SonarDiscoverFade>();
        if (obj)
        {
            obj.startFade();
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainSounds : MonoBehaviour {

	public AudioClip gamePlaySound;
	public AudioClip gamePlayTenseSound;
	public AudioClip gameOverSound;

	private AudioSource audioSource;
	private AudioSource audioSourceTense;
	private string playingSound;

	// Use this for initialization
	void Start () {
		playingSound = "Game Play";
		audioSource = GetComponents<AudioSource> ()[0];
		audioSourceTense = GetComponents<AudioSource> ()[1];
	}
	
	// Update is called once per frame
	void Update () {
//		if (actualSound && !audioSource.isPlaying) {
//			audioSource.PlayOneShot (actualSound, 1.0f);
//		}
		if (!audioSource.isPlaying) {
			if (playingSound == "Game Play") {
				PlayGamePlaySound ();
			} else if (playingSound == "Game Over") {
				PlayGameOverSound ();
			}
		}
	}

	public void PlayGamePlaySound () {
		playingSound = "Game Play";
		audioSource.Stop ();
		audioSourceTense.Stop ();
		audioSource.PlayOneShot (gamePlaySound, 1.0f);
		audioSourceTense.PlayOneShot (gamePlayTenseSound, 1.0f);
		audioSourceTense.volume = 0.0f;
	}

	public void SetGamePlayTenseVolume (float volume) {
		audioSourceTense.volume = volume;
	}

	public void PlayGameOverSound () {
		playingSound = "Game Over";
		audioSource.Stop ();
		audioSourceTense.Stop ();
		audioSource.PlayOneShot (gameOverSound, 1.0f);
	}
}

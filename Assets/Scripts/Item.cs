﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour {

	public GameObject objectToFollow = null;

    public float energy = 10f;
    public bool full_energy = false;
	public float iconAlertDistance = 10.0f;
	public AudioClip collectSound;

	private bool collected;
	private HUDIconAlert energyIcon;
	private AudioSource audioSource;

	void Start () {
		collected = false;
		audioSource = GetComponent<AudioSource> ();
		GameObject obj = GameObject.Find("EnergyIcon");
		energyIcon = obj.GetComponent<HUDIconAlert>();
	}
	
	void Update () {
		if (objectToFollow) 
		{
			Vector3 follow_position = objectToFollow.GetComponent<Transform> ().position;
			float distance = (gameObject.transform.position - follow_position).magnitude;
			if (distance <= iconAlertDistance) {
				energyIcon.TurnOnAlert ();
			}
		}
		else
		{
			objectToFollow = GameObject.Find("Submarine");
		}

		if (collected && !audioSource.isPlaying) {
			gameObject.SetActive(false);
			Destroy(gameObject);
		}
	}

    void OnTriggerEnter2D(Collider2D collider) {
        Player player = collider.gameObject.GetComponent<Player>();

		if (player && !collected)
        {
			collected = true;
            player.AddEnergy(energy, full_energy);
			audioSource.PlayOneShot (collectSound, 1.0f);
        }
    }
}
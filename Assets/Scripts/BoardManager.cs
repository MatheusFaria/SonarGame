﻿using System.Collections;
using System.Collections.Generic;
using System;
using Random = UnityEngine.Random;
using UnityEngine;

public class BoardManager : MonoBehaviour
{
    public int numberStones = 10;
    public int numberEnemySubs = 5;
    public int numberEnergy = 8;
    public int numberMonster = 8;

    public int maxRadius = 100;
    public int minRadius = 10;
    public float minDistance = 5f;

    public GameObject player;
    public GameObject[] stoneTiles;
    public GameObject[] enemySubTiles;
    public GameObject[] energyTiles;
    public GameObject[] monsterTiles;

    public float respawnTime = 5f;
    private float lastTime;
    private Player playerScript;

    private Transform boardHolder;
    private List<GameObject> objects;

    void InitialiseList()
    {
        objects = new List<GameObject>();
    }

    void BoardSetup ()
    {
        boardHolder = new GameObject("Board").transform;
		PlaceObjectsRandom ();
    }

	void PlaceObjectsRandom () {
		PlaceObjectsRandom(numberStones, stoneTiles);
		PlaceObjectsRandom(numberEnemySubs, enemySubTiles);
		PlaceObjectsRandom(numberEnergy, energyTiles);
		PlaceObjectsRandom(numberMonster, monsterTiles);
	}

    void PlaceObjectsRandom(int number_of_objects, GameObject[] tileArray)
    {
        for (int i = 0; i < number_of_objects; ++i)
        {
            int x = 0, y = 0, count = 0;

            do
            {
                count++;
                if (count == 10) break;

                x = Random.Range(-maxRadius, maxRadius);
                if (Mathf.Abs(x) < minRadius)
                    x = ((int)Mathf.Sign(x)) * minRadius;
                x += (int)objects[0].transform.position.x;

                y = Random.Range(-maxRadius, maxRadius);
                if (Mathf.Abs(y) < minRadius)
                    y = ((int)Mathf.Sign(y)) * minRadius;
               y += (int) objects[0].transform.position.y;

            } while (IsThereObjectAt(x, y));

            if (count == 10) continue;

            Vector3 randomPosition = new Vector3(x, y, 0);

            GameObject tileChoice = tileArray[Random.Range(0, tileArray.Length)];
            GameObject instance = Instantiate(tileChoice, randomPosition, Quaternion.identity) as GameObject;
            objects.Add(instance);

            instance.transform.SetParent(boardHolder);
        }
    }

    bool IsThereObjectAt(int x, int y)
    {
        foreach(GameObject obj in objects)
        {
            float distance = (obj.GetComponent<Transform>().position - new Vector3(x, y, 0)).magnitude;
            if (distance <= minDistance) return true;
        }
        return false;
    }

    public void SetupScene()
    {
        InitialiseList();

        GameObject player_obj = Instantiate(player, new Vector3(0, 0, 0), Quaternion.identity);
        player_obj.name = "Submarine";
        objects.Add(player_obj);
        playerScript = player_obj.GetComponent<Player>();

        BoardSetup();
        lastTime = Time.time;
    }

    void Update()
    {
        if(playerScript && !playerScript.isDead && Time.time - lastTime >= respawnTime)
        {
            ClearFarAway();

            PlaceObjectsRandom(numberStones/2, stoneTiles);
            PlaceObjectsRandom(numberEnemySubs/2, enemySubTiles);
            PlaceObjectsRandom(numberEnergy/2, energyTiles);
            PlaceObjectsRandom(numberMonster/2, monsterTiles);

            lastTime = Time.time;
        }
    }

    void ClearFarAway()
    {
        List<GameObject> objects_bkp = new List<GameObject>();

        foreach (GameObject obj in objects)
        {
            if (!obj) continue;

            float distance = (obj.GetComponent<Transform>().position - objects[0].transform.position).magnitude;
            if (distance < maxRadius)
            {
                objects_bkp.Add(obj);
            }
            else
            {
                obj.SetActive(false);
                Destroy(obj);
            }
        }

        objects = objects_bkp;
    }

	public void RestartMap () {
		DestroyObjects ();
		PlaceObjectsRandom ();
	}

	public void DestroyObjects () {
		GameObject player = null;
		foreach (GameObject obj in objects) {
			if (obj.tag != "Player") {
				Destroy (obj);
			} else {
				player = obj;
			}
		}
		objects.Clear ();
		objects.Add (player);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;
    public BoardManager boardScript;
    public GameObject hud;
    public Player player;
    public GameObject gameOverPanel;
	public GameObject crackedGlass;
	public Text scoreText;
    private GameObject playerGameObject;
	[HideInInspector] public MainSounds mainSounds;
	[HideInInspector] public GameGlitchEffect gameGlitchEffect;

	private float gameTime;
	private bool isGamePlay = false;
    private bool isGameOver = false;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    void Start()
	{
		mainSounds = GetComponent<MainSounds>();
        boardScript = GetComponent<BoardManager>();
		gameGlitchEffect = GetComponent<GameGlitchEffect>();
        InitGame();
    }

    void InitGame()
    {
		gameTime = 0f;
		isGamePlay = true;
        Instantiate(hud, new Vector3(0, 0, 0), Quaternion.identity);
        boardScript.SetupScene();
        playerGameObject = GameObject.Find("Submarine");
        player = playerGameObject.GetComponent<Player>();
    }

    void Update()
    {
        CheckGameOver();
        TryRestart();
		UpdateGameScore ();
    }

    void CheckGameOver()
    {
        if (player.isDead && !isGameOver)
        {
            isGameOver = true;
			isGamePlay = false;
			boardScript.DestroyObjects ();
			mainSounds.PlayGameOverSound ();
			Instantiate(crackedGlass, playerGameObject.transform.position, Quaternion.identity);
			Instantiate(gameOverPanel, playerGameObject.transform.position, Quaternion.identity);
        }
    }

    void TryRestart()
    {
        if (isGameOver)
        {
            if (Input.GetButtonDown("Submit"))
            {
                SceneManager.LoadScene("Gameplay");
            }
        }
    }

	void UpdateGameScore(){
		if (isGamePlay) {
			gameTime += Time.deltaTime;
			int secondsTime = (int)gameTime;
			int minutes = secondsTime / 60;
			int seconds = secondsTime % 60;
			string score = string.Format ("{0:00}:{1:00}", minutes, seconds);
			scoreText.text = score;
		}
	}
}

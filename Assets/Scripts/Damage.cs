﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damage : MonoBehaviour {

    public float damage = 20f;
    public bool hitKill = false;
	public AudioClip damageSound;

	private AudioSource audioSource;

    void Start () {
		audioSource = GetComponent<AudioSource> ();
	}
	
	void Update () {
		
	}

	public void PlayDamageSound () {
		audioSource.PlayOneShot (damageSound, 1.0f);
	}
}

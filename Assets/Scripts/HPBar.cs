﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HPBar : MonoBehaviour {

    public Sprite[] HPStates;

    void Start () {
		
	}
	
	void Update () {
		
	}

    public void setHP(float HP) {
        int animation = 20 - ((int)HP / 5);
        gameObject.GetComponent<SpriteRenderer>().sprite = HPStates[animation];
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUDIconAlert : MonoBehaviour {

	public static int ON = 1;
	public static int OFF = 0;

	public float timeWithAlert = 0.5f;
	public Sprite[] iconState;

	private bool isTurnOn;
	private float alertTime;

	// Use this for initialization
	void Start () {
		isTurnOn = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (isTurnOn) {
			alertTime += Time.deltaTime;
			if (alertTime >= timeWithAlert) {
				TurnOffAlert ();
			}
		}
	}

	public void TurnOnAlert() {
		if (!isTurnOn) {
			isTurnOn = true;
			alertTime = 0.0f;
			gameObject.GetComponent<SpriteRenderer> ().sprite = iconState [ON];
		}
	}

	void TurnOffAlert() {
		isTurnOn = false;
		gameObject.GetComponent<SpriteRenderer> ().sprite = iconState [OFF];
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAlert : MonoBehaviour {

	public GameObject objectToFollow = null;
	public bool hasGlitch = true;
	public float iconAlertDistance = 10.0f;
	public float soundDistance = 7.0f;
	public float glitchDistance = 5.0f;
	public AudioClip sound;

	private HUDIconAlert misteryIcon;
	private AudioSource audioSource;

	// Use this for initialization
	void Start () {
		audioSource = GetComponent<AudioSource> ();
		GameObject obj = GameObject.Find("MisteryIcon");
		misteryIcon = obj.GetComponent<HUDIconAlert>();
	}
	
	// Update is called once per frame
	void Update () {
		if (objectToFollow) 
		{
			Vector3 follow_position = objectToFollow.GetComponent<Transform> ().position;
			float distance = (gameObject.transform.position - follow_position).magnitude;
			if (distance <= iconAlertDistance) {
				misteryIcon.TurnOnAlert ();
			}
			if (distance <= soundDistance) {
				if (audioSource && !audioSource.isPlaying) audioSource.PlayOneShot (sound, 1.0f);
			}
			if (distance <= glitchDistance) {
				if(hasGlitch)
					GameManager.instance.gameGlitchEffect.StartGlitchEffect ();
			}
		}
		else
		{
			objectToFollow = GameObject.Find("Submarine");
		}
	}
}

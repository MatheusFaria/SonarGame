﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SonarDiscoverFade : MonoBehaviour {

	public float fadeInDuration = 0.3f;
    public float fadeOutDuration = 5.0f;

	private float actualMinimum;
    private float minimum = 0f;
    private float maximum = 1f;
    private float startTime;
    private SpriteRenderer sprite;
    private bool isFading = false;
    private bool fadeIn = true;

    void Start()
    {
		actualMinimum = minimum;
        sprite = gameObject.GetComponent<SpriteRenderer>();
        sprite.color = new Color(sprite.color.r, sprite.color.g, sprite.color.b, minimum);
    }

    void Update()
    {
        if (isFading)
        {
            if (fadeIn)
            {
                float t = (Time.time - startTime) / fadeInDuration;
				float alpha = Mathf.SmoothStep(actualMinimum, maximum, t);
                sprite.color = new Color(sprite.color.r, sprite.color.g, sprite.color.b, alpha);

                if (alpha == maximum)
                {
                    fadeIn = false;
                }
            }
            else
            {
                float t = (Time.time - startTime) / fadeOutDuration;
                float alpha = Mathf.SmoothStep(maximum, minimum, t);
                sprite.color = new Color(sprite.color.r, sprite.color.g, sprite.color.b, alpha);

                if (alpha == minimum)
                {
                    isFading = false;
                }
            }
        }
    }

    public void startFade()
    {
		actualMinimum = !isFading ?  minimum : sprite.color.a;
		startTime = Time.time;
		isFading = true;
		fadeIn = true;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public float speed = 1f;
    public float HP = 100f;
    public float sonarCost = 5f;
    public float sonarCooldown = 2f;
    private float sonarPressedTime = 0f;
	[HideInInspector] public bool isDead = false;
	[HideInInspector] public bool isStopped = true;
    public GameObject sonar;
	public AudioClip soundIgnition;
	public AudioClip soundShootSonar;
    public float consumeEnergyTime = 5f;
    public float consumeEnergyCost = 5f;

    private float consumeTimestamp = 0f;

	private float maxHP;
    private HPBar hpbar;
	private AudioSource audioSource;

	void Start () {
		maxHP = HP;
        GameObject obj;
		obj = GameObject.Find("HPBar");
		sonarPressedTime = Time.time - sonarCooldown;
		audioSource = GetComponent<AudioSource> ();

        consumeTimestamp = Time.time;

        if (obj)
        {
            hpbar = obj.GetComponent<HPBar>();
        }
    }
	
	void Update () {
		CheckDeath ();
        if(isDead)
        {
            gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
        }
        else
        {
            Controls();
            if (Time.time - consumeTimestamp >= consumeEnergyTime)
            {
                consumeTimestamp = Time.time;
                SufferDamage(consumeEnergyCost);
            }
        }

        hpbar.setHP(HP);
	}

    void Controls() {
        Camera cam = Camera.main;
        cam.transform.Rotate(new Vector3(0, 0, -Input.GetAxis("Horizontal")));

        float angle = Mathf.Deg2Rad * (cam.transform.eulerAngles.z + 90);
        Vector3 dir = new Vector3(Mathf.Cos(angle), Mathf.Sin(angle), 0f);

        Vector3 delta_position = new Vector3(0, Input.GetAxis("Vertical"), 0);
        transform.position += Mathf.Sign(Input.GetAxis("Vertical")) * dir * this.speed * delta_position.magnitude * Time.deltaTime;

        if (Input.GetButtonDown("Jump") && (Time.time - sonarPressedTime >= sonarCooldown) )
        {
            ShootSonar();
            sonarPressedTime = Time.time;
        }

		Ignition (delta_position);
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.gameObject.layer == LayerMask.NameToLayer("BlockingLayer"))
        {
            Damage damage = collision.collider.gameObject.GetComponent<Damage>();
			if (damage) 
			{
				damage.PlayDamageSound ();
				SufferDamage (damage.damage, damage.hitKill);
			}
        }
    }

    void SufferDamage(float damage, bool hitKill=false)
    {
        this.HP -= damage;
        if (hitKill) this.HP = 0;

        Debug.Log("SufferDamage: Ouch (" + damage + ")");
        CheckDeath();
    }

    void CheckDeath()
    {
        if (HP <= 0)
        {
            HP = 0;
            this.isDead = true;
            Debug.Log("CheckDeath: I'm dead");
        }
		UpdateTenseSoundVolume ();
    }

    void ShootSonar()
    {
        HP -= sonarCost;

		audioSource.PlayOneShot (soundShootSonar, 1.0f);
        GameObject sonar_obj = Instantiate(sonar, transform.position, Quaternion.identity) as GameObject;
        sonar_obj.GetComponent<FollowGameObject>().objectToFollow = gameObject;

        CheckDeath();
    }

	void Ignition (Vector3 delta_position)
	{
		if (delta_position == Vector3.zero) {
			isStopped = true;
		} else {
			if (isStopped) {
				audioSource.PlayOneShot (soundIgnition, 1.0f);
			}
			isStopped = false;
		}
	}

	void UpdateTenseSoundVolume () {
		if (HP > maxHP * 0.20f) {
			GameManager.instance.mainSounds.SetGamePlayTenseVolume (0.0f);
		} else if (HP > maxHP * 0.15f) {
			GameManager.instance.mainSounds.SetGamePlayTenseVolume (0.25f);
		} else if (HP > maxHP * 0.10f) {
			GameManager.instance.mainSounds.SetGamePlayTenseVolume (0.65f);
		} else {
			GameManager.instance.mainSounds.SetGamePlayTenseVolume (1.0f);
		}
	}

    public void AddEnergy(float energy, bool full_energy) {

        Debug.Log("Got more energy " + energy);

        if (full_energy)
        {
            HP = 100;
        }
        else {
            HP = Mathf.Min(HP+energy, 100);
        }
		UpdateTenseSoundVolume ();
    }
}

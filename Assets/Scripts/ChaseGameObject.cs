﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChaseGameObject : MonoBehaviour {

    public GameObject objectToFollow = null;
    public float speed = 1f;
    public float chaseRadius = 5f;

    void Start () {}
	
	void Update () {
		if (objectToFollow)
        {
            Vector3 follow_position = objectToFollow.GetComponent<Transform>().position;

            // Chase only if it is in the radius
            if ((gameObject.transform.position - follow_position).magnitude <= chaseRadius)
            {
                Vector3 newPosition = Vector3.MoveTowards(gameObject.transform.position, follow_position, speed * Time.deltaTime);
                gameObject.GetComponent<Rigidbody2D>().MovePosition(newPosition);
            }
        }
        else
        {
            objectToFollow = GameObject.Find("Submarine");
        }
	}
}

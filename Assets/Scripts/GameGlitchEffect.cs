﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameGlitchEffect : MonoBehaviour {

	public float timeWithGlitch = 1.5f;
	public float timeToNextGlitch = 1.0f;

	public AudioClip sound;

	[HideInInspector] public bool isStarted;
	private bool finishedGlitch;
	private float glitchTime;
	private AudioSource audioSource;
	private GlitchEffect glitchEffect;

	void Start () {
		isStarted = false;
		finishedGlitch = false;
		glitchTime = 0.0f;
		audioSource = GetComponents<AudioSource> ()[2];
		glitchEffect = Camera.main.GetComponent<GlitchEffect> ();
	}

	void Update () {
		if (Input.GetKeyDown (KeyCode.G)) {
			StartGlitchEffect ();
		}

		if (isStarted && !finishedGlitch) {
			glitchTime += Time.deltaTime;
			if (glitchTime >= timeWithGlitch) {
				StopGlitchEffect ();
				finishedGlitch = true;
			} else if (!audioSource.isPlaying) {
				SetGlitchIntensity (1.0f);
				audioSource.PlayOneShot (sound, 1.0f);
			}
		} else if (finishedGlitch) {
			glitchTime += Time.deltaTime;
			if (glitchTime >= timeToNextGlitch) {
				finishedGlitch = false;
				glitchTime = 0.0f;
			}
		}
	}

	void SetGlitchIntensity (float intensity) {
		glitchEffect.intensity = intensity;
		glitchEffect.flipIntensity = intensity;
		glitchEffect.colorIntensity = intensity;
	}

	public void StartGlitchEffect () {
		if (!isStarted) {
			isStarted = true;
			glitchTime = 0.0f;
			audioSource.volume = 1.0f;
		}
	}

	public void StopGlitchEffect () {
		isStarted = false;
		glitchTime = 0.0f;
		audioSource.Stop ();
		audioSource.volume = 0.0f;
		SetGlitchIntensity (0.0f);
	}
}

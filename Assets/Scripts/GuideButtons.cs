﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuideButtons : MonoBehaviour {

	public bool fadeOut = true;
	public float fadeOutDuration = 10.0f;
	public GameObject guideButton;
	public SpriteRenderer sprite;
	public Vector3 positionGuide = new Vector3(0f, -2.62f, 0f);

	void Start () {
		sprite = gameObject.GetComponent<SpriteRenderer> ();
		//guideButton = GameObject.Find("Guide");
		//Instantiate(guideButton, positionGuide, Quaternion.identity);
	}


	void Update () {

		if (fadeOut) {
			float t = (Time.time) / fadeOutDuration;
			float alpha = Mathf.SmoothStep(1, 0, t);
			sprite.color = new Color(sprite.color.r, sprite.color.g, sprite.color.b, alpha);

			if (alpha == 0)
			{
				fadeOut = false;
			}
		}
			
	}
}

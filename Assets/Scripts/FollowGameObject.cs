﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowGameObject : MonoBehaviour {

    public GameObject objectToFollow = null;
    
	void Start () {
		
	}
	
	void Update () {
		if (objectToFollow)
        {
            Vector3 obj_position = objectToFollow.GetComponent<Transform>().position;
            transform.position = new Vector3(obj_position.x, obj_position.y, transform.position.z);
        }
        else
        {
            objectToFollow = GameObject.Find("Submarine");
        }
    }
}

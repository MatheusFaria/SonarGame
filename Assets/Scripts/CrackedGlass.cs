﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrackedGlass : MonoBehaviour {

	public AudioClip crackingSound;
	private AudioSource audioSource;
	void Start () {
		audioSource = GetComponent<AudioSource> ();
		audioSource.PlayOneShot (crackingSound, 1f);
	}

	void Update () {
		
	}
}

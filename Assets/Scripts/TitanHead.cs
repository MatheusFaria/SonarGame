﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitanHead : MonoBehaviour {

	public float desapearTime = 1.0f;

	private GameObject submarine;
	// Use this for initialization
	void Start () {
		Invoke ("AutoRemove", desapearTime);
	}
	
	// Update is called once per frame
	void Update () {
		transform.eulerAngles = new Vector3(0, 0, Camera.main.transform.eulerAngles.z);
		transform.Translate(new Vector2 (0, 0));
	}

	void AutoRemove() {
		Destroy (gameObject);
	}
}

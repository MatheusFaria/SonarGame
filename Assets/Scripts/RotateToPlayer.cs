﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateToPlayer : MonoBehaviour {

    private GameObject player;

	void Start () {
        player = GameObject.Find("Submarine");
	}
	
	void Update () {
        AdjustRotation();
	}

    public void AdjustRotation() {

        if (!player) return;

        Quaternion q = Quaternion.LookRotation(player.transform.position - transform.position, Vector3.forward);
        q.x = 0; q.y = 0;
        transform.rotation = q;

    }
}

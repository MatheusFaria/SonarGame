﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Oni : MonoBehaviour {

	public static float TELEPORT_MAX = 20.0f;
	public static float TELEPORT_MIN = 10.0f;

	public GameObject objectToFollow = null;
	public GameObject titanHead;
	public float iconAlertDistance = 10.0f;

	private HUDIconAlert warningIcon;

	// Use this for initialization
	void Start () {
		GameObject obj = GameObject.Find("WarningIcon");
		warningIcon = obj.GetComponent<HUDIconAlert>();
	}
	
	// Update is called once per frame
	void Update () {
		if (objectToFollow) {
			Vector3 follow_position = objectToFollow.GetComponent<Transform> ().position;
			float distance = (gameObject.transform.position - follow_position).magnitude;
			if (distance <= iconAlertDistance) {
				warningIcon.TurnOnAlert ();
			}
		} else {
			objectToFollow = GameObject.Find("Submarine");
		}
	}

	void OnTriggerEnter2D (Collider2D collider) {
		Player player = collider.gameObject.GetComponent<Player>();

		if (player)
		{
			GameManager.instance.gameGlitchEffect.StartGlitchEffect ();
			int randomNumber = Random.Range (1, 11);
			if (randomNumber % 2 == 0) {
				TeleportFollowedObject ();
			} else {
				ShowTitanHead ();
			}
			Destroy (gameObject);
		}
	}

	void TeleportFollowedObject () {
		GameManager.instance.boardScript.RestartMap ();
	}

	void ShowTitanHead() {
		GameObject submarine = GameObject.Find ("Submarine");
		Instantiate(titanHead, submarine.transform);
	}
}
